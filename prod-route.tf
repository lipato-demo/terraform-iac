resource "aws_route53_record" "prod-python_a_record" {
  zone_id = data.aws_route53_zone.public_zone.id
  name    = "${var.prod-subdomain}.${data.aws_route53_zone.public_zone.name}"
  type    = "CNAME"
  ttl     = "30"
  records = [aws_elb.prod-python-elb.dns_name]
}

resource "aws_route53_record" "prod-python-cert-validations-record" {
  allow_overwrite = true
  name            = tolist(aws_acm_certificate.prod-python-cert.domain_validation_options)[0].resource_record_name
  records         = [ tolist(aws_acm_certificate.prod-python-cert.domain_validation_options)[0].resource_record_value ]
  type            = tolist(aws_acm_certificate.prod-python-cert.domain_validation_options)[0].resource_record_type
  zone_id  = data.aws_route53_zone.public_zone.id
  ttl      = 60
}

resource "aws_acm_certificate_validation" "prod-python-cert-validation" {
  certificate_arn         = aws_acm_certificate.prod-python-cert.arn
  validation_record_fqdns = [ aws_route53_record.prod-python-cert-validations-record.fqdn ]
}

resource "aws_acm_certificate" "prod-python-cert" {
  domain_name       = "${var.prod-subdomain}.${data.aws_route53_zone.public_zone.name}"
  validation_method = "DNS"
  tags = {
    Environment = "prod python-app record"
  }
  lifecycle {
    create_before_destroy = true
  }
}
