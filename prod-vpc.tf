resource "aws_vpc" "prod-vpc" {
  cidr_block       = var.prod_vpc_cidr_block
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "prod-vpc"
  }
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "prod-public_subnet" {
  vpc_id     = aws_vpc.prod-vpc.id
  cidr_block = var.prod_public_subnet_cidr_block
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "prod public subnet"
  }
}

resource "aws_internet_gateway" "prod-public-igw" {
  vpc_id = aws_vpc.prod-vpc.id
  tags = {
    Name = "prod python-app internet-igw"
  }
}

resource "aws_route_table" "prod-public_subnet-rt" {
  vpc_id = aws_vpc.prod-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.prod-public-igw.id
  }
  tags = {
    Name = "prod public-subnet rt"
  }
}

resource "aws_route_table_association" "prod-public_routes" {
  subnet_id      = aws_subnet.prod-public_subnet.id
  route_table_id = aws_route_table.prod-public_subnet-rt.id
}

resource "aws_subnet" "prod-private_subnet" {
  vpc_id     = aws_vpc.prod-vpc.id
  cidr_block = var.prod_private_subnet_cidr_block
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "prod private subnet"
  }
}

resource "aws_eip" "prod-nat-eip" {
  vpc   = true
  tags = {
    Name = "prod-nat-gw"
  }
}

resource "aws_nat_gateway" "prod-nat" {
  allocation_id = aws_eip.prod-nat-eip.id
  subnet_id     = aws_subnet.prod-public_subnet.id
  tags = {
    Name = "prod-nat-gw"
  }
}

resource "aws_route_table" "prod-private_subnet_rt" {
  vpc_id = aws_vpc.prod-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.prod-nat.id
  }
  tags = {
    Name = "prod-private-subnet-rt"
  }
}

resource "aws_route_table_association" "prod-private_routes" {
  route_table_id = aws_route_table.prod-private_subnet_rt.id
  subnet_id      = aws_subnet.prod-private_subnet.id
}