output "prod_elb_dns_name" {
  description = "prod elb dns_name"
  value       = aws_elb.prod-python-elb.dns_name
}

output "prod_domain" {
  description = "prod elb dns_name"
  value       = "https://${var.prod-subdomain}.${var.owner-domain}"
}

output "dev_domain" {
  description = "dev dns_name"
  value       = "http://${var.dev-subdomain}.${var.owner-domain}:8080"
}