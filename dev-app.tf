resource "aws_security_group" "dev-python_sg" {
  name        = "dev python-app sg"
  description = "dev python-app security group"
  vpc_id      = aws_vpc.dev-vpc.id

  dynamic "ingress" {
    for_each = ["22", "80", "443", "8080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "dev python-app sg"
  }
}

resource "aws_instance" "dev-python_app" {
  ami             = aws_ami_from_instance.python_image.id
  instance_type   = var.aws_ami_instance_type
  subnet_id       = aws_subnet.dev-private_subnet.id
  vpc_security_group_ids = [aws_security_group.dev-python_sg.id]
  user_data = templatefile("files/setup-python.sh.tpl", {
    nginx_config  = file("files/python-app-site.conf")
  })
  tags = {
    Name = "dev-python-app instance"
  }
}

resource "aws_eip" "dev-pyth_eip" {
  instance = aws_instance.dev-python_app.id
  vpc      = true
  tags = {
    Name = "dev-python-eip"
  }
}