terraform {
  backend "s3" {
    bucket = "demo-python-app-bucket"
    key = "dev/network/terraform.tfstate"
    region = "eu-central-1"
  }
}

variable "private_key_path" {
  type  = string
  default = ""
}

variable "private_pubkey_path" {
  type  = string
  default = ""
}

variable "aws_region" {
  type       = string
  default = "eu-central-1"
}

data "aws_ami" "latest_centos" {
  owners      = ["679593333241"]
  most_recent = true
  filter {
    name   = "name"
    values = ["Centos8 Clean*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

variable "owner-domain" {
  type  = string
  default = "lipato.dev"
}

variable "prod-subdomain" {
  type  = string
  default = "python-app"
}

variable "dev-subdomain" {
  type  = string
  default = "dev-python-app"
}

variable "prod_vpc_cidr_block" {
  type        = string
  default     = "10.1.0.0/21"
}

variable "dev_vpc_cidr_block" {
  type        = string
  default     = "10.2.0.0/21"
}

variable "prod_public_subnet_cidr_block" {
  type        = string
  default     = "10.1.0.0/24"
}

variable "prod_private_subnet_cidr_block" {
  type        = string
  default     = "10.1.1.0/24"
}

variable "dev_public_subnet_cidr_block" {
  type        = string
  default     = "10.2.0.0/24"
}

variable "dev_private_subnet_cidr_block" {
  type        = string
  default     = "10.2.1.0/24"
}

variable "aws_ami_instance_type" {
  type       = string
  default = "t3.micro"
}
