#!/bin/bash
bash -c 'echo "${nginx_config}" >> /etc/nginx/conf.d/python-app-site.conf'
git clone -b master https://gitlab.com/lipato-demo/python-app.git /app
cd /app && pip3 install -r requirements.txt
service nginx reload
su - www-data -c "cd /app && python3 -m gunicorn --bind 127.0.0.1:8000 wsgi:app"