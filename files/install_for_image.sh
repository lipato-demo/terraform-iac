sudo yum -y install epel-release git python3 nginx
sudo groupadd www-data
sudo useradd -m -g www-data www-data
sudo systemctl enable nginx
sudo service nginx start
sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
sudo firewall-cmd --permanent --zone=public --add-port=8080/tcp
sudo firewall-cmd --permanent --zone=public --add-port=443/tcp
sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
sudo firewall-cmd --reload
sudo setsebool -P httpd_can_network_connect 1