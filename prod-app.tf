resource "aws_security_group" "prod-python_sg" {
  name        = "prod python-app sg"
  description = "prod python-app security group"
  vpc_id     = aws_vpc.prod-vpc.id

  dynamic "ingress" {
    for_each = ["80", "443", "8080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  ingress {
    from_port = 8
    to_port = 0
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "prod python-app sg"
  }
}

resource "aws_launch_configuration" "prod-python_lconf" {
  name_prefix     = "prod-python-app elb instance"
  image_id           = aws_ami_from_instance.python_image.id
  instance_type = var.aws_ami_instance_type
  security_groups = [aws_security_group.prod-python_sg.id]
  user_data = templatefile("files/setup-python.sh.tpl", {
    nginx_config  = file("files/python-app-site.conf")
  })
}

resource "aws_autoscaling_group" "prod-python-asgroup" {
  name                 = "prod python asgroup"
  launch_configuration = aws_launch_configuration.prod-python_lconf.name
  vpc_zone_identifier  = [aws_subnet.prod-private_subnet.id]
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  health_check_type    = "ELB"
  load_balancers       = [aws_elb.prod-python-elb.name]
  tag {
    key                 = "Name"
    value               = "prod-python-app elb instance"
    propagate_at_launch = true
  }
}

resource "aws_elb" "prod-python-elb" {
  name               = "prod-python-elb"
  security_groups    = [aws_security_group.prod-python_sg.id]
  subnets            = [aws_subnet.prod-public_subnet.id,aws_subnet.prod-private_subnet.id]
  listener {
    instance_port      = 8080
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = aws_acm_certificate.prod-python-cert.id
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }
  tags = {
    Name = "prod python-app ELB"
  }
}
