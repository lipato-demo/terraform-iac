provider "aws" {
  region = var.aws_region
}

data "aws_route53_zone" "public_zone" {
  name = var.owner-domain
}
