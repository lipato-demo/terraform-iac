resource "aws_key_pair" "deploy_key" {
  key_name   = "deploy-key"
  public_key = file(var.private_pubkey_path)
}

resource "aws_instance" "dev-create-image" {
  ami             = data.aws_ami.latest_centos.id
  instance_type   = var.aws_ami_instance_type
  subnet_id       = aws_subnet.dev-public_subnet.id
  vpc_security_group_ids = [aws_security_group.dev-create-image_sg.id]
  key_name = aws_key_pair.deploy_key.key_name
  associate_public_ip_address = true

  provisioner "remote-exec" {
    inline = [
      file("files/install_for_image.sh")
    ]
  }

  connection {
    type = "ssh"
    user = "ec2-user"
    private_key = file(var.private_key_path)
    host = aws_instance.dev-create-image.public_ip
    agent = false
  }

  tags = {
    Name = "dev create-image instance"
  }
}

resource "aws_ami_from_instance" "python_image" {
  name               = "centos8-clean.python.nginx"
  source_instance_id = aws_instance.dev-create-image.id
}

resource "aws_security_group" "dev-create-image_sg" {
  name        = "dev create-image sg"
  description = "dev create-image security group"
  vpc_id      = aws_vpc.dev-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "dev create-image sg"
  }
}