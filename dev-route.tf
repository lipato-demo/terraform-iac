resource "aws_route53_record" "dev-python_a_record" {
  zone_id = data.aws_route53_zone.public_zone.id
  name    = "${var.dev-subdomain}.${data.aws_route53_zone.public_zone.name}"
  type    = "CNAME"
  ttl     = "30"
  records = [aws_eip.dev-pyth_eip.public_ip]
}