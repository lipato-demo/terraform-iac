resource "aws_vpc" "dev-vpc" {
  cidr_block           = var.dev_vpc_cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "dev-vpc"
  }
}

resource "aws_subnet" "dev-public_subnet" {
  vpc_id     = aws_vpc.dev-vpc.id
  cidr_block = var.dev_public_subnet_cidr_block

  tags = {
    Name = "dev public subnet"
  }
}

resource "aws_internet_gateway" "dev-public-igw" {
  vpc_id = aws_vpc.dev-vpc.id
  tags = {
    Name = "dev python-app internet-igw"
  }
}

resource "aws_route_table" "dev-public_subnet-rt" {
  vpc_id = aws_vpc.dev-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dev-public-igw.id
  }
  tags = {
    Name = "dev public-subnet rt"
  }
}

resource "aws_route_table_association" "dev-public_routes" {
  subnet_id      = aws_subnet.dev-public_subnet.id
  route_table_id = aws_route_table.dev-public_subnet-rt.id
}

resource "aws_subnet" "dev-private_subnet" {
  vpc_id     = aws_vpc.dev-vpc.id
  cidr_block = var.dev_private_subnet_cidr_block

  tags = {
    Name = "dev private subnet"
  }
}

resource "aws_eip" "dev-nat-eip" {
  vpc   = true
  tags = {
    Name = "dev-nat-gw"
  }
}

resource "aws_nat_gateway" "dev-nat" {
  allocation_id = aws_eip.dev-nat-eip.id
  subnet_id     = aws_subnet.dev-public_subnet.id
  tags = {
    Name = "dev-nat-gw"
  }
}

resource "aws_route_table" "dev-private_subnet_rt" {
  vpc_id = aws_vpc.dev-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.dev-nat.id
  }
  tags = {
    Name = "dev-private-subnet-rt"
  }
}

resource "aws_route_table_association" "dev-private_routes" {
  route_table_id = aws_route_table.dev-private_subnet_rt.id
  subnet_id      = aws_subnet.dev-private_subnet.id
}